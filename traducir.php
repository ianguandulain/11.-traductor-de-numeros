<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get posted data
//$numero = json_decode(file_get_contents("php://input"));
$numero = $_GET['numero'];
$met = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// make sure data is not empty
if(
    $met == 'GET'
){
    http_response_code(201);

    switch($numero) {
      case 1:
        $ruso = "один";
      break;
      case 2:
        $ruso = "два";
      break;
      case 3:
        $ruso = "три";
      break;
      case 4:
        $ruso = "четыре";
      break;
      case 5:
        $ruso = "пять";
      break;
      case 6:
        $ruso = "шесть";
      break;
      case 7:
        $ruso = "семь";
      break;
      case 8:
        $ruso = "восемь";
      break;
      case 9:
        $ruso = "девять";
      break;
      case 10:
        $ruso = "десять";
      break;
      default:
        $ruso = "Error!";
    break;
    }
    echo json_encode(array(
        "Method:" => $met,
        "Número" => $numero,
        "Pronunciación en ruso" => $ruso
      )
    );
}else{
    http_response_code(503);
    echo json_encode(array("message" => "No data was received"));
}
?>
